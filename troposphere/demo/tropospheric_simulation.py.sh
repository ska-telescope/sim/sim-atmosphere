#!/bin/bash
#!
python ../../atmospheric_simulation.py --context s3sky --frequency 1.36e9 --rmax 1e4 \
--flux_limit 0.03 --show True --export_images True --pbtype MID_GAUSS --nworkers 4 \
--memory 16 --integration_time 30 --use_agg False --type_atmosphere troposphere \
--time_range -0.1 0.05 --time_chunk 900 \
--screen /Users/timcornwell/Code/sim-atmosphere/screens/mid_screen_5000.0r0_0.033rate.fits \
--serial True --r0 5000 50000 --imaging_context ng --use_natural True --zerow True \
--npixel 5120 --pbradius 1.6 | tee tropospheric_simulation.log
